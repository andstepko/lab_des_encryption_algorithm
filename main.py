import functools


def text_to_binary(text):
    return ''.join("{0:08b}".format(ord(char), 'b') for char in text)


def block_to_text(block_64):
    result = ""
    for i in range(8):
        block_8 = block_64[i * 8:i * 8 + 8]
        block_8_str = ''.join(block_8)
        int_value = int(block_8_str, 2)
        result += chr(int_value)
    return result


def get_left_32(block):
    result = [None] * 32
    for i in range(0, 32):
        result[i] = block[i]
    return result


def get_right_32(block):
    result = [None] * 32
    for i in range(0, 32):
        result[i] = block[i + 32]
    return result


def initial_permutation(block_64):
    print("initial_permutation.block==>" + str(block_64))
    print("initial_permutation.block.len()==>" + str(len(block_64)))
    permutation_matrix = [58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,
                          62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8,
                          57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3,
                          61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7]
    result = [None]*64
    index = 0
    for number in permutation_matrix:
        result[index] = block_64[number - 1]
        index += 1
    return result


def final_permutation(block_64):
    matrix = [40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31,
              38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29,
              36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27,
              34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25]

    result = [None]*64
    for i in range(64):
        result[i] = block_64[matrix[i] - 1]
    return result


def xor(chain1, chain2):
    result_len = min(len(chain1), len(chain2))
    result = [None] * result_len
    for i in range(result_len):
        result[i] = str(int(chain1[i] != chain2[i]))
    return result


def encrypt_following_block(block_64, key_48):
    old_right_32 = get_right_32(block_64)

    function_product = f(old_right_32, key_48)
    new_right_32 = xor(get_left_32(block_64), function_product)
    new_block_64 = old_right_32 + new_right_32
    return new_block_64


# region f
def f(block_32, key_48):
    block_48 = expand_32_to_48_e(block_32)

    xor_porduction_48 = xor(block_48, key_48)
    chain_32 = ''
    for i in range(8):
        cur_block_6 = get_6bit_block(xor_porduction_48, i)
        cur_block_4 = convert_6_to_4_bit_block_s(cur_block_6, i)
        chain_32 += cur_block_4

    return permutation_32_p(chain_32)


def get_6bit_block(block_48, index):
    result = [None] * 6
    start_index = index * 6
    for i in range(6):
        result[i] = block_48[start_index + i]
    return result


def convert_6_to_4_bit_block_s(block_6, table_index):
    # region tables
    s1 = [
        [14, 4, 13, 1, 2, 5, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
        [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
        [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
        [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13]
    ]
    s2 = [
        [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
        [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
        [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
        [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9]
    ]
    s3 = [
        [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
        [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
        [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
        [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]
    ]
    s4 = [
        [7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
        [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
        [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
        [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14]
    ]
    s5 = [
        [2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
        [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
        [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
        [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3]
    ]
    s6 = [
        [12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
        [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
        [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
        [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13]
    ]
    s7 = [
        [4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
        [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
        [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
        [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12]
    ]
    s8 = [
        [13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
        [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
        [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
        [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]
    ]
    tables = [s1, s2, s3, s4, s5, s6, s7, s8]
    # endregion tables

    a = int(block_6[0] + block_6[5], 2)
    int_str = functools.reduce(lambda x, y:x + y, block_6[1:5], '')
    # b = int(block_6[1:5], 2)
    b = int(int_str, 2)

    table_cell = tables[table_index][a][b]
    binary_result = "{0:04b}".format(table_cell)
    return binary_result


def expand_32_to_48_e(block_32):
    matrix = [32, 1, 2, 3, 4, 5,
              4, 5, 6, 7, 8, 9,
              8, 9, 10, 11, 12, 13,
              12, 13, 14, 15, 16, 17,
              16, 17, 18, 19, 20, 21,
              20, 21, 22, 23, 24, 25,
              24, 25, 26, 27, 28, 29,
              28, 29, 30, 31, 32, 1]

    result = [None] * 48
    for i in range(48):
        result[i] = block_32[matrix[i] - 1]
    return result


def permutation_32_p(block_32):
    matrix = [16, 7, 20, 21, 29, 12, 28, 17,
              1, 15, 23, 26, 5, 18, 31, 10,
              2, 8, 24, 14, 32, 27, 3, 9,
              19, 13, 30, 6, 22, 11, 4, 25]

    result = [None] * 32
    for i in range(32):
        result[i] = block_32[matrix[i] - 1]
    return result
# endregion f


def encrypt(block_64, key_64):
    current_block_64 = initial_permutation(block_64)

    for i in range(16):
        key_i_48 = get_cur_key(key_64, i + 1)
        current_block_64 = encrypt_following_block(current_block_64, key_i_48)
        print_statistics(current_block_64)

    return final_permutation(current_block_64)


def decrypt_following_block(block_64, key_48):
    old_left_32 = get_left_32(block_64)

    function_product = f(old_left_32, key_48)
    new_left_32 = xor(get_right_32(block_64), function_product)
    return new_left_32 + old_left_32


# region keys
def cycle_left_shift(chain, positions_to_shift):
    return chain[positions_to_shift:len(chain)] + chain[0:positions_to_shift]


def get_cur_key(key_64, natural_index):
    c0_permutation = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18,
                      10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36]
    d0_permutation = [63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22,
                      14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4]

    c0_d0_56 = [None] * 56
    for i in range(28):
        c0_d0_56[i] = key_64[c0_permutation[i] - 1]
        c0_d0_56[28 + i] = key_64[d0_permutation[i] - 1]

    shifts = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1]
    ci_di_56 = c0_d0_56
    for i in range(natural_index):
        ci_di_56 = cycle_left_shift(ci_di_56, shifts[i])
    return get_48bit_key(ci_di_56)


def get_48bit_key(key_56):
    matrix = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4,
              26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40,
              51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32]

    result = [None] * 48
    for i in range(48):
        result[i] = key_56[matrix[i] - 1]
    return result

# endregion keys


# Decrypt
def decrypt(block_64, key_64):
    current_block_64 = initial_permutation(block_64)

    for i in range(16):
        key_i_48 = get_cur_key(key_64, 16 - i)
        current_block_64 = decrypt_following_block(current_block_64, key_i_48)

    return final_permutation(current_block_64)


# Statistics
def print_statistics(block_64):
    byte_blocks = [None] * 8
    for i in range(8):
        byte_blocks[i] = block_64[i * 8:i * 8 + 8]
    result_string = ""
    for i in range(8):
        count = 0
        for j in range(8):
            if int(byte_blocks[j][i]):
                count += 1
        result_string += str(count / 8) + "; "
    print(result_string)


test_block_64 = text_to_binary("Hello wo")
test_key_64 = text_to_binary("My nameA")

encrypted_block_64 = encrypt(test_block_64, test_key_64)
decrypted_block_64 = decrypt(encrypted_block_64, test_key_64)

print(block_to_text(encrypted_block_64))
print(block_to_text(decrypted_block_64))

# print_statistics(test_block_64)
# print_statistics(encrypted_block_64)
